ALTER TABLE `media` ADD `hires_video_url` VARCHAR(255) NOT NULL AFTER `standard_url`;
ALTER TABLE `media` ADD `link` VARCHAR(255) NOT NULL AFTER `user_profile_picture`, ADD `caption` VARCHAR(255) NOT NULL AFTER `link`, ADD `username` VARCHAR(255) NOT NULL AFTER `caption`, ADD `full_name` VARCHAR(255) NOT NULL AFTER `username`, ADD `user_id` INT(11) NOT NULL AFTER `full_name`;

#vote table
CREATE TABLE `votes` (`gram_id` varchar(32) NOT NULL, `votes` smallint(3) NOT NULL DEFAULT '0', KEY `gram_id` (`gram_id`), KEY `votes` (`votes`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
