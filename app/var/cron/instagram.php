<?php

//get configs
require dirname(dirname(dirname(__FILE__))) .'/config.php';

//use vendor lib
require dirname(dirname(dirname(__FILE__))) .'/vendors/instagram.class.php';

/**
 * deal with empty location names
 *
 */
function null_names($d)
{	
  if(isset($d->location->name)) return (string)$d->location->name;
	
  return ''; 
}

function null_videos($d)
{ 
  if(isset($d->videos)) return (string)$d->videos->standard_resolution->url;
  
  return ''; 
}

function null_caption($d)
{ 
  if(isset($d->caption->text)) return (string)$d->caption->text;
  
  return ''; 
}

/**
 * save to db
 *
 */
function save_item($d)
{
  global $dbh; var_dump($d);
		
  $sql = array(
    'q' => "REPLACE INTO media SET
      gram_id              = ? ,
      created_time         = ? ,
      location             = ? ,
      standard_url         = ? ,
      hires_video_url      = ? ,
      user_profile_picture = ? ,
      link                 = ? ,
      caption              = ? ,
      username             = ? ,
      full_name            = ? ,
      user_id              = ? 
    ",
    'p' => array(
      $d->id,
      $d->created_time,
      null_names($d),
      $d->images->standard_resolution->url,
      null_videos($d),
      $d->user->profile_picture,
      $d->link,
      null_caption($d),
      $d->user->username,
      $d->user->full_name,
      $d->user->id
  ));

  $p = $dbh->prepare($sql['q']);
  $p->execute($sql['p']);

  init_votes($d->id);

  echo 'saved: '. date('Y-m-d h:i:s A',$d->created_time ) .' '. $d->id .' '. $d->user->full_name ."\n" ;
}

function init_votes($id)
{
  global $dbh;

  $check = sprintf("SELECT gram_id FROM votes WHERE gram_id='%s' LIMIT 1", $id);
  $res = $dbh->query($check);

  //Todo: error check & prune
  if ($res->rowCount() == 0 )
  {
    $sql = sprintf("INSERT INTO votes SET gram_id='%s' ", $id);
    $dbh->query($sql);
  }

}

// Initialize class for public requests
$instagram = new Instagram( INSTAGRAM_KEY );

// Get near media, centered on 19th/Broadway
$lat 	= 37.805206598;
$lng 	= -122.270879745;
$m		= 1300; 
$p 		= $instagram->searchMedia($lat,$lng,$m);

if(!is_object($p)) die('None returned');

// Display results
foreach ($p->data as $data) 
{
	
	save_item($data);
	
	//var_dump($data->created_time);
	//var_dump($data->location->name); //can be null
	//var_dump($data->id);
	//var_dump($data->images->standard_resolution->url); //612x612
	//echo "<img src=\"{$data->images->standard_resolution->url}\">";
	//echo "<img src=\"{$data->user->profile_picture}\">";
	//echo "<hr><br/><br/>";
}
