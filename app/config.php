<?php

//force local settings
ini_set('allow_url_fopen', 1 );
ini_set('default_charset', 'utf-8');
date_default_timezone_set('America/Los_Angeles');
error_reporting(E_ALL);
set_time_limit(0);

if (!defined('INSTAGRAM_KEY')) define("INSTAGRAM_KEY", '');
if (INSTAGRAM_KEY === '') trigger_error("Instagram API Key not set", E_USER_ERROR);

define("CACHE", dirname(dirname(__FILE__)) .'/public_html/images/cache');
if (!is_writable( CACHE )) trigger_error("Cache not writeable: " . CACHE , E_USER_ERROR);


/**
 * MySQL database info
 *
 */
$db = array
(
	'host' => 'localhost',
	'name' => 'instaoak',
	'user' => 'root',
	'pass' => 'root'
);

/**
 * connect to db
 *
 */
try
{
	$dbh = new PDO('mysql:host='. $db['host'] .';dbname='. $db['name'] , $db['user'], $db['pass'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")  );
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
	catch(PDOException $e)
{
	echo $e->getMessage();
}

